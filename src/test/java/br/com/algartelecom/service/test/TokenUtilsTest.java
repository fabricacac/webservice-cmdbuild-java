package br.com.algartelecom.service.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.algartelecom.model.AuthenticatedTO;
import br.com.algartelecom.utils.TokenClient;

@ContextConfiguration(locations={"classpath*:applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TokenUtilsTest {
	
	@Autowired
	private TokenClient token;

	@Test
	public void verifyGetToken(){
		AuthenticatedTO to = token.getToken();
		Assert.assertNotNull(to);
		Assert.assertNotNull(to.get_id());
	}
	
	@Test
	public void readSessionTokenExist(){
		String id = token.readSession();
		Assert.assertNotNull(id);
	}
	
	@Test
	public void deleteToken(){
		AuthenticatedTO to = token.getToken();
		token.deleteSession();
		Assert.assertNotNull(to);
	}
	
	@Test
	public void readTokenNotAuthorization(){
		AuthenticatedTO to = token.getToken();
		to.set_id("580kd9ksi2r1neqqcch0p6kxxx");
		String id = token.readSession();
		Assert.assertNotNull(id);
	}

}
