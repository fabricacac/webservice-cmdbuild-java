package br.com.algartelecom.dao.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.algartelecom.dao.client.ClientDAO;
import junit.framework.Assert;

@SuppressWarnings("deprecation")
@ContextConfiguration(locations={"classpath*:applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class DAOTest {

	@Autowired
	private ClientDAO clientDAO;
	
	@Test
	public void testConnectPostgre(){
		clientDAO.getAllClients(10);	
	}

	@Test
	public void replaceCNPJ(){
		String cnpj = "25.630.740/0001-32";
		cnpj =  cnpj.replaceAll("\\W", "");
		Assert.assertEquals(cnpj, "25630740000132");
	}
}
