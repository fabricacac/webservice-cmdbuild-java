package br.com.algartelecom.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.algartelecom.service.ClassesService;
import br.com.algartelecom.utils.Controller;
import br.com.algartelecom.utils.Preconditions;

@Service
@Scope("request")
@Path("/classes")
public class ClassesController extends Controller{

	@Autowired
	private ClassesService service;

	/**
	 * Reading of all classes
	 * @return
	 * {
	*	 meta : {
	*				total : ...
	*	}
	*	data : [
	*		{
	*				_id : Asset,
	*				name : Asset,
	*				description : Asset,
	*				parent : Class,
	*				prototype : true
	*			},
	*			...
	*			{
	*			...
	*		}
	*			]
	*		}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllClasses(){
		return service.getAllClasses();
	}
	
	/**
	 * Read of the details of a class
	 * @return
	 * {
	 *	data : {
	 *	_     id : Asset,
     *        name : Asset,
     *        description : Asset,
     *        description_attribute_name : Description,
     *        parent : Class,
     *        prototype : true
     *    }
     * }
	 */
	@GET
	@Path("{idClasses}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getReadClass(@PathParam("idClasses") String idClasses){
		Preconditions.checkNotBlank(idClasses, "'idClasses'");
		return service.readDetailsClass(idClasses);
	}
	
	/**
	 * Read of all attributes of a class
	 * @return
	 * {
	 *   meta : {
     *     total : ...
     *   },
     * data : [
     *        {
     *         lookupType : null,
     *         index : 1,
     *         group : General data,
     *         unique : false,
     *         scale : null,
     *         editorType : null,
     *         targetClass : null,
     *         active : true,
     *         description : Code,
     *         type : string,
     *         displayableInList : true,
     *         values : [],
     *         length : 100,
     *         name : Code,
     *        _id : Code,
     *         filter : null,
     *        mandatory : false,
     *        precision : null,
     *        defaultValue" : null,
     *        inherited" : true
     *       },
     *...
     *       { 
     *         ...
     *       }
     * ]
     *}
	 */
	@GET
	@Path("/{idClasses}/attributes")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAttributesClass(@PathParam("idClasses") String idClasses){
		Preconditions.checkNotBlank(idClasses, "'idClasses'");
		return service.readAttributesClass(idClasses);
	}

}
