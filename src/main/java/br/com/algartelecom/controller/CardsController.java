package br.com.algartelecom.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.algartelecom.service.CardsService;
import br.com.algartelecom.utils.Controller;
import br.com.algartelecom.utils.Preconditions;

@Service
@Scope("request")
@Path("/card")
public class CardsController extends Controller{

	@Autowired
	private CardsService service;
	
	/**
	 * Creation of a card
	 * @param idClasses *Id da Classe para inserir o card.
	 * @param object *Dados a ser inseridos no card.
	 * @return
	 * 
	 * {
	 *		data : 3217
	 * }
	 */
	@POST
	@Path("{idClasses}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String creationCard(@PathParam("idClasses") String idClasses,Object object){
		Preconditions.checkNotBlank(idClasses, "'idClasses'");
		Preconditions.checkNotNull(object, "'object'");
		return service.createCards(idClasses, object);
	}
	
	/**
	 * Reading of all cards
	 * @param idClasses *Id da Classe para consulta dos cards.
	 * @return
	 * meta : {
	 *   	total : ...
	 *	} ,
	 *	data : [
	 *		    {
	 *				...
	 *			}
	 *		]
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllCards(@QueryParam("idClasses") String idClasses){
		Preconditions.checkNotBlank(idClasses, "'idClasses'");
		return service.listAllCards(idClasses);
	}
	
	/**
	 * Reading of the details of a card
	 * @param idCard *Id do card a ser pesquisado.
	 * @param idClasses *Id da classe que pertence o card.
	 * @return
	 * 
	 * data : { ... }
	 */
	@GET
	@Path("/details")
	@Produces(MediaType.APPLICATION_JSON)
	public String readingDetailsCard(@QueryParam("idCard") Integer idCard, @QueryParam("idClasses") String idClasses){
		Preconditions.checkNotNull(idCard, "'idCard'");
		Preconditions.checkNotBlank(idClasses, "'idClasses'");
		return service.readingDetailsCard(idClasses,idCard);
	}
	
	/**
	 * Update of a card
	 * @param idClasses *Id da Classe que o card pertence
	 * @param idCard *Id do card a ser atualizado
	 * @param object *Dados a ser atualizados.
	 * @return
	 * HTTP/1.1 204 No Content
	 * 
	 */
	@PUT
	@Path("{idClasses}/{idCard}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String updateCard(@PathParam("idClasses") String idClasses, @PathParam("idCard") Integer idCard,Object object){
		Preconditions.checkNotNull(idCard, "'idCard'");
		Preconditions.checkNotBlank(idClasses, "'idClasses'");
		Preconditions.checkNotNull(object, "'object'");
		return service.updateCard(idClasses, idCard, object);
	}
	

}
