package br.com.algartelecom.controller;


import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.algartelecom.model.Cliente;
import br.com.algartelecom.model.ClienteTO;
import br.com.algartelecom.service.client.ClientService;
import br.com.algartelecom.utils.CRUDResponse;
import br.com.algartelecom.utils.Controller;
import br.com.algartelecom.utils.Preconditions;

@Service
@Scope("request")
@Path("/client")
public class ClientController extends Controller{

	@Autowired
	private ClientService service;

	/**
	 * Lista todos os clientes
	 * @return
	 */
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Cliente> getAllClients(){
		return service.getAllClientes();
	}

	/**
	 *  Retorna informação de um cliente.
	 * @param serviceGroupID *ID do grupo 
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ClienteTO getClient(@QueryParam("serviceGroupID") String serviceGroupID){
		Preconditions.checkNotBlank("serviceGroupID", serviceGroupID);
		return service.getClientByDocument(serviceGroupID);
	}

	/**
	 * Cria um novo cliente e seus produtos.
	 * Parametros obrigatórios: Service Group ID
	 * @param cliente 
	 * @return
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CRUDResponse saveClient(ClienteTO cliente){
		return service.insertClient(cliente);
	}
}
