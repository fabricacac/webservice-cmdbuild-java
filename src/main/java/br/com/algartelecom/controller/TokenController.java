package br.com.algartelecom.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.algartelecom.model.AuthenticatedTO;
import br.com.algartelecom.utils.Controller;
import br.com.algartelecom.utils.GenericJson;
import br.com.algartelecom.utils.TokenClient;

@Service
@Scope("request")
@Path("/token")
public class TokenController extends Controller{
	
	@Autowired
	private TokenClient client;
	
	/**
	 * Get Token Session
	 * @return
	 * @throws Exception
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public GenericJson<AuthenticatedTO> getToken() throws Exception{
		return ok(client.getToken());
	}
	
	/**
	 * Verify Token Session
	 * @return
	 */
	@Path("/verify")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public GenericJson<String> getIdPost(){
		return ok(client.readSession());
	}

}
