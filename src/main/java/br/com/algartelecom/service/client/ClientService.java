package br.com.algartelecom.service.client;

import java.util.List;

import br.com.algartelecom.model.Cliente;
import br.com.algartelecom.model.ClienteTO;
import br.com.algartelecom.utils.CRUDResponse;

public interface ClientService {

	List<Cliente> getAllClientes();

	CRUDResponse insertClient(ClienteTO cliente);

	ClienteTO getClientByDocument(String serviceGroupId);

}
