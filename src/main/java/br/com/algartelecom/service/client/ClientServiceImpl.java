package br.com.algartelecom.service.client;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.algartelecom.dao.client.ClientDAO;
import br.com.algartelecom.model.Action;
import br.com.algartelecom.model.Cliente;
import br.com.algartelecom.model.ClienteTO;
import br.com.algartelecom.model.OrderType;
import br.com.algartelecom.model.Produto;
import br.com.algartelecom.model.Server;
import br.com.algartelecom.model.StorageServer;
import br.com.algartelecom.model.StorageVirtualMachine;
import br.com.algartelecom.model.VirtualMachine;
import br.com.algartelecom.utils.CRUDResponse;
import br.com.algartelecom.utils.ErroDetails;

@Service
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientDAO dao;

	@Override
	public List<Cliente> getAllClientes() {
		return dao.getAllClients(null);
	}

	@Override
	public ClienteTO getClientByDocument(String serviceGroupId) {
		ClienteTO result = new ClienteTO();
		Produto client =  dao.getProductGroupID(serviceGroupId);
		if(client != null){
			result.setProduct_Details(client);
			result.setClient_Details(client.getClienteProduto());
		}
		return result;
	}

	@Override
	public CRUDResponse insertClient(ClienteTO cliente) {
		CRUDResponse crud = new CRUDResponse();
		List<Server> server = Server.getServerObject(cliente);
		List<StorageServer> storageServer = StorageServer.getObjectStorage(cliente);
		List<VirtualMachine> virtualMachine = VirtualMachine.getObjectVirtualMachine(cliente);
		List<StorageVirtualMachine> storageVirtualMachine = StorageVirtualMachine.getObjectStorageVirtualMachine(cliente);
		if(cliente.getOrder_Type().equals(OrderType.NEW)){
			this.insertCliente(crud, cliente, server, storageServer, virtualMachine, storageVirtualMachine);
		}
		if(cliente.getOrder_Type().equals(OrderType.MODIFY)){
			this.updateCliente(crud, cliente, server, virtualMachine);
		}
		if(cliente.getOrder_Type().equals(OrderType.DESCONECT)){
			this.desconectCliente(crud, cliente, server, virtualMachine);
		}
		return crud;
	}

	private CRUDResponse insertCliente(CRUDResponse crud ,ClienteTO cliente, List<Server> server, List<StorageServer> storageServer, List<VirtualMachine> virtualMachine, List<StorageVirtualMachine> storageVirtualMachine){
		CRUDResponse client = dao.getClientByDocument(cliente.getClient_Details().getCPF_CNPJ());
		if(client != null && client.getCliente() != null){
			crud.setStatus(1);
			crud.setErro_Deatils(ErroDetails.setObjectDetails("409", "Conflict, client exist. Document: " + cliente.getClient_Details().getCPF_CNPJ()));
			return client;
		}
		dao.saveClient(cliente.getClient_Details());
		Cliente cl = dao.getClientByDescription(cliente.getClient_Details().getDescription());
		cliente.getProduct_Details().setDescription(cliente.getProduct_Details().getService_Group_Instance_ID());
		cliente.getProduct_Details().setClienteProduto(cl);
		dao.saveProduto(Produto.getObjectProduto(cliente));
		cl = dao.getClientByDescription(cliente.getClient_Details().getDescription());
		if(server != null && !server.isEmpty()){
			for(int i = 0 ; i < server.size(); i++){
				server.get(i).setDescription(server.get(i).getService_ID());
				server.get(i).setProdutoServidor(cl.getProduct());
				dao.saveServer(server.get(i));
				if(storageServer != null && !storageServer.isEmpty()){
					if(server.get(i).getDados_de_Storage() != null){
						cl = dao.getClientByDescription(cliente.getClient_Details().getDescription());
						for(int j = 0; j < server.get(i).getDados_de_Storage().size(); j++){	
							storageServer.get(j).setServidorStorage(cl.getProduct().getServidor().get(i));
							dao.saveServerStorage(storageServer.get(j));
						}
					}

				}
			}
		}
		if(virtualMachine != null && !virtualMachine.isEmpty()){
			for(int i = 0; i < virtualMachine.size(); i++){
				virtualMachine.get(i).setProdutoMaquinaVirtual(cl.getProduct());
				dao.saveVirtualMachine(virtualMachine.get(i));
				if(storageVirtualMachine != null && !storageVirtualMachine.isEmpty()){
					if(virtualMachine.get(i).getDados_de_Storage() != null){
						cl = dao.getClientByDescription(cliente.getClient_Details().getDescription());
						for(int j = 0; j < virtualMachine.get(i).getDados_de_Storage().size(); j++){	
							virtualMachine.get(i).getDados_de_Storage().get(j).setMaquinaVirtualStorage(cl.getProduct().getMaquina_Virtual().get(i));
							dao.saveStorageVirtualMachine(virtualMachine.get(i).getDados_de_Storage().get(j));
						}
					}
				}
			}
		}
		crud.setStatus(0);
		crud.setErro_Deatils(new ErroDetails());
		return crud;
	}

	private CRUDResponse updateCliente(CRUDResponse crud ,ClienteTO cliente, List<Server> server, List<VirtualMachine> virtualMachine){
		CRUDResponse rep = dao.getClientByDocument(cliente.getClient_Details().getDescription());
		if(rep.getStatus() != null){
			return rep;
		}
		Cliente cl = dao.getClientByDescription(cliente.getClient_Details().getDescription());
		cl = Cliente.generationObjectUpdate(cliente, cl);
		if(cl.getProduct().getServidor() != null){
			for(int i = 0; i < server.size(); i++){
				if(server.get(i).equals(Action.DELETE)){
					cl.getProduct().getServidor().get(i).setSTATUS("INATIVO");
					if(cl.getProduct().getServidor().get(i).getDados_de_Storage() != null){
						for(int z = 0; z < cl.getProduct().getServidor().get(i).getDados_de_Storage().size(); z++){
							cl.getProduct().getServidor().get(i).getDados_de_Storage().get(z).setSTATUS("INATIVO");
						}
					}
				}
			}
		}
		if(cl.getProduct().getMaquina_Virtual() != null){
			for(int i = 0; i < virtualMachine.size(); i++){
				if(virtualMachine.get(i).equals(Action.DELETE)){
					cl.getProduct().getMaquina_Virtual().get(i).setSTATUS("INATIVO");
					if(cl.getProduct().getMaquina_Virtual().get(i).getDados_de_Storage() != null){
						for(int z = 0; z < cl.getProduct().getMaquina_Virtual().get(i).getDados_de_Storage().size(); z++){
							cl.getProduct().getMaquina_Virtual().get(i).getDados_de_Storage().get(z).setSTATUS("INATIVO");
						}
					}
				}
			}
		}
		dao.updateClient(cl);
		crud.setStatus(0);
		crud.setErro_Deatils(new ErroDetails());
		return crud;
	}

	private CRUDResponse desconectCliente(CRUDResponse crud ,ClienteTO cliente, List<Server> server, List<VirtualMachine> virtualMachine){
		CRUDResponse rep = dao.getClientByDocument(cliente.getClient_Details().getDescription());
		if(rep.getStatus() != null){
			return rep;
		}
		Cliente client = dao.getClientByDescription(cliente.getClient_Details().getDescription());
		if(client.getProduct().getServidor() != null){
			for(Server s : client.getProduct().getServidor()){
					s.setSTATUS("INATIVO");
					if(s.getDados_de_Storage() != null){
						for(StorageServer st : s.getDados_de_Storage()){
							st.setSTATUS("INATIVO");
						}
					}
			}
		}
		if(client.getProduct().getMaquina_Virtual()!= null){
			for(VirtualMachine v : client.getProduct().getMaquina_Virtual()){
				v.setSTATUS("INATIVO");
				if(v.getDados_de_Storage() != null){
					for(StorageVirtualMachine sv : v.getDados_de_Storage()){
						sv.setSTATUS("INATIVO");
					}
				}
			}
		}
		cliente.getProduct_Details().setSTATUS("INATIVO");
		dao.updateClient(client);
		crud.setStatus(0);
		crud.setErro_Deatils(new ErroDetails());
		return crud;
	}

}
