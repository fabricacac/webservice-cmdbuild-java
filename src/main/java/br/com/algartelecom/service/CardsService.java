package br.com.algartelecom.service;

public interface CardsService {

	String listAllCards(String card);

	String createCards(String idClasses, Object object);

	String readingDetailsCard(String idClasses, Integer idCard);

	String updateCard(String idClasses, Integer idCard, Object object);

}
