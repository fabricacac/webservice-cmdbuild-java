package br.com.algartelecom.service;

import javax.ws.rs.core.UriBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import br.com.algartelecom.utils.ConstantsURL;
import br.com.algartelecom.utils.RestClientURL;
import br.com.algartelecom.utils.TokenClient;

@Service
public class ClassesServiceImpl implements ClassesService{
	
	@Autowired
	private RestClientURL restClient;
	
	@Autowired
	private TokenClient tokenClient;
	
	@Override
	public String getAllClasses(){
		try {
			WebResource service = restClient.getClient().resource(UriBuilder.fromUri(restClient.getUrl()+ConstantsURL.getAllClasses).build());
			ClientResponse client = restClient.get(service, tokenClient.readSession());
			String jsonResult = client.getEntity(String.class);
			return jsonResult;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;	
	}
	
	@Override
	public String readDetailsClass(String idClasses){
		try {
			WebResource service = restClient.getClient().resource(UriBuilder.fromUri(String.format(restClient.getUrl()+ConstantsURL.readDetailsClass,idClasses)).build());
			ClientResponse client = restClient.get(service, tokenClient.readSession());
			String jsonResult = client.getEntity(String.class);
			return jsonResult;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public String readAttributesClass(String idClasses){
		try {
			WebResource service = restClient.getClient().resource(UriBuilder.fromUri(String.format(restClient.getUrl()+ConstantsURL.readAttributesClass,idClasses)).build());
			ClientResponse client = restClient.get(service, tokenClient.readSession());
			String jsonResult = client.getEntity(String.class);
			return jsonResult;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
