package br.com.algartelecom.service;

import javax.ws.rs.core.UriBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import br.com.algartelecom.utils.ConstantsURL;
import br.com.algartelecom.utils.RestClientURL;
import br.com.algartelecom.utils.TokenClient;

@Service
public class CardsServiceImpl implements CardsService{
	
	@Autowired
	private RestClientURL restClient;
	
	@Autowired
	private TokenClient tokenClient;
	
	@Override
	public String createCards(String idClasses, Object object){
		try {
			WebResource service = restClient.getClient().resource(UriBuilder.fromUri(String.format(restClient.getUrl()+ConstantsURL.createCard, idClasses)).build());
			ClientResponse client = restClient.post(service, tokenClient.readSession(), object);
			String jsonResult = client.getEntity(String.class);
			return jsonResult;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public String listAllCards(String card){
		try {
			WebResource service = restClient.getClient().resource(UriBuilder.fromUri(String.format(restClient.getUrl()+ConstantsURL.listAllCards, card)).build());
			ClientResponse client = restClient.get(service, tokenClient.readSession());
			String jsonResult = client.getEntity(String.class);
			return jsonResult;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public String readingDetailsCard(String idClasses, Integer idCard){
		try {
			WebResource service = restClient.getClient().resource(UriBuilder.fromUri(String.format(restClient.getUrl()+ConstantsURL.readingDetailsCard, idClasses,idCard)).build());
			ClientResponse client = restClient.get(service, tokenClient.readSession());
			String jsonResult = client.getEntity(String.class);
			return jsonResult;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public String updateCard(String idClasses, Integer idCard, Object object){
		try {
			WebResource service = restClient.getClient().resource(UriBuilder.fromUri(String.format(restClient.getUrl()+ConstantsURL.updateCard ,idClasses, idCard)).build());
			ClientResponse client = restClient.put(service, tokenClient.readSession(), object);
			String jsonResult = client.getEntity(String.class);
			return jsonResult;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


}
