package br.com.algartelecom.service;

public interface ClassesService {

	String getAllClasses();

	String readDetailsClass(String name);

	String readAttributesClass(String name);

}
