package br.com.algartelecom.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.SelectBeforeUpdate;

@Entity
@SelectBeforeUpdate
@SequenceGenerator(name = "nome_seq_virtual", sequenceName = "class_seq", allocationSize = 1, initialValue = 1)
@Table(name="\"Maquina_Virtual\"")
public class VirtualMachine implements Serializable{

	private static final long serialVersionUID = -3673707088414041625L;

	@Id
	@GeneratedValue(generator="nome_seq_virtual")
	@Column(name="\"Id\"", unique=true, nullable=false)
	private Integer id;

	@Column(name = "\"Provisioning_ID\"", nullable = true)
	private String Provisioning_ID;

	@Column(name = "\"Service_ID\"", nullable = false)
	private String Service_ID;

	@Column(name = "\"STATUS\"", nullable = true)
	private String STATUS;

	@Column(name = "\"Host_name\"", nullable = true)
	private String Host_name;

	@JsonIgnore
	@Column(name = "\"Description\"", nullable = false)
	private String Description;

	@Transient
	@JsonIgnore
	private Action action;

	@ManyToOne
	@JoinColumn(name="\"ID_Service_Group_Instance_ID\"")
	@JsonBackReference
	private Produto produtoMaquinaVirtual;

	@OneToMany(mappedBy = "maquinaVirtualStorage", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT) 
	@JsonManagedReference
	private List<StorageVirtualMachine> Dados_de_Storage;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		if(this.Service_ID != null){
			Description =  this.Service_ID;
		}else{
			Description = description;
		}
	}

	public String getProvisioning_ID() {
		return Provisioning_ID;
	}

	public void setProvisioning_ID(String provisioning_ID) {
		Provisioning_ID = provisioning_ID;
	}

	public String getService_ID() {
		return Service_ID;
	}

	public void setService_ID(String service_ID) {
		Service_ID = service_ID;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getHost_name() {
		return Host_name;
	}

	public void setHost_name(String host_name) {
		Host_name = host_name;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public Produto getProdutoMaquinaVirtual() {
		return produtoMaquinaVirtual;
	}

	public void setProdutoMaquinaVirtual(Produto produtoMaquinaVirtual) {
		this.produtoMaquinaVirtual = produtoMaquinaVirtual;
	}

	public List<StorageVirtualMachine> getDados_de_Storage() {
		return Dados_de_Storage;
	}

	public void setDados_de_Storage(List<StorageVirtualMachine> dados_de_Storage) {
		Dados_de_Storage = dados_de_Storage;
	}

	public static List<VirtualMachine> getObjectVirtualMachine(ClienteTO to){
		if(to.getProduct_Details() != null && to.getProduct_Details().getMaquina_Virtual() != null && !to.getProduct_Details().getMaquina_Virtual().isEmpty()){
			return to.getProduct_Details().getMaquina_Virtual();
		}
		return null;
	}

}
