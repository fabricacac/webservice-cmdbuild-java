package br.com.algartelecom.model;

import java.io.Serializable;
import java.util.List;

import net.sf.json.JSONObject;

public class AuthenticatedTO implements Serializable{
	
	private static final long serialVersionUID = 2152541027930872218L;
	private String username;
	private String role;
	private List<String> availableRoles;
	private String _id;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public List<String> getAvailableRoles() {
		return availableRoles;
	}
	public void setAvailableRoles(List<String> availableRoles) {
		this.availableRoles = availableRoles;
	}
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	
	@SuppressWarnings("unchecked")
	public static AuthenticatedTO setReturnObject(JSONObject jsonOutput){
		AuthenticatedTO r = new AuthenticatedTO();
		r.set_id(jsonOutput.getJSONObject("data").get("_id").toString());
		r.setAvailableRoles((List<String>) jsonOutput.getJSONObject("data").get("availableRoles"));
		r.setUsername(jsonOutput.getJSONObject("data").get("username").toString());
		r.setRole(jsonOutput.getJSONObject("data").get("role").toString());
		return r;
	}

}
