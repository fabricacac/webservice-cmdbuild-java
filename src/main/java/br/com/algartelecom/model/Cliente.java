package br.com.algartelecom.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.annotations.SelectBeforeUpdate;

@Entity
@SelectBeforeUpdate
@SequenceGenerator(name = "nome_seq", sequenceName = "class_seq", allocationSize = 1, initialValue = 1)
@Table(name = "\"Cliente\"")
public class Cliente implements Serializable {

    private static final long serialVersionUID = 5534009955587635387L;

    @Id
    @GeneratedValue(generator = "nome_seq")
    @Column(name = "\"Id\"", unique = true, nullable = false)
    private Integer id;

    @Column(name = "\"Client_Name\"", nullable = true)
    private String Client_Name;

    @Column(name = "\"Email\"", nullable = true)
    private String Email;

    @Column(name = "\"Customer_ID\"", nullable = true)
    private String Customer_ID;

    @Column(name = "\"CPF_CNPJ\"", nullable = false)
    private String CPF_CNPJ;

    @JsonIgnore
    @Column(name = "\"Description\"", nullable = false)
    private String Description;
    
    @Column(name = "\"Status\"", nullable = true)
    private String STATUS;

    @OneToOne(mappedBy = "clienteProduto", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonManagedReference
    @JsonIgnore
    private Produto product;
    

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClient_Name() {
		return Client_Name;
	}

	public void setClient_Name(String client_Name) {
		Client_Name = client_Name;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getCustomer_ID() {
		return Customer_ID;
	}

	public void setCustomer_ID(String customer_ID) {
		Customer_ID = customer_ID;
	}

	public String getCPF_CNPJ() {
		return CPF_CNPJ;
	}

	public void setCPF_CNPJ(String cPF_CNPJ) {
		CPF_CNPJ = cPF_CNPJ;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		if(this.CPF_CNPJ != null){
			Description = this.CPF_CNPJ;
		}else{
			Description = description;
		}
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public Produto getProduct() {
		return product;
	}

	public void setProduct(Produto product) {
		this.product = product;
	}
	
	public static Cliente generationObjectUpdate(ClienteTO to , Cliente cliente){
		if(to.getProduct_Details() != null){
			to.getProduct_Details().setId(cliente.getProduct().getId());
			to.getProduct_Details().setDescription(cliente.getProduct().getDescription());
			if(to.getProduct_Details().getServidor() != null){
				for(int i = 0; i < to.getProduct_Details().getServidor().size(); i++){
					for(int z = 0; z < cliente.getProduct().getServidor().size(); z++){
						to.getProduct_Details().getServidor().get(i).setId(cliente.getProduct().getServidor().get(z).getId());
						to.getProduct_Details().getServidor().get(i).setDescription(cliente.getProduct().getServidor().get(z).getDescription());
						if(to.getProduct_Details().getServidor().get(i).getDados_de_Storage() != null){
							for(int j = 0; j < to.getProduct_Details().getServidor().get(i).getDados_de_Storage().size(); j++){
								to.getProduct_Details().getServidor().get(i).getDados_de_Storage().get(j).setId(cliente.getProduct().getServidor().get(i).getDados_de_Storage().get(j).getId());
							}
						}
					}
				}
			}
			if(to.getProduct_Details().getMaquina_Virtual() != null){
				for(int i = 0; i < to.getProduct_Details().getMaquina_Virtual().size(); i++){
					for(int z = 0; z < cliente.getProduct().getMaquina_Virtual().size(); z++){
						to.getProduct_Details().getMaquina_Virtual().get(i).setId(cliente.getProduct().getMaquina_Virtual().get(z).getId());
						to.getProduct_Details().getMaquina_Virtual().get(i).setDescription(cliente.getProduct().getMaquina_Virtual().get(z).getDescription());
						if(to.getProduct_Details().getMaquina_Virtual().get(i).getDados_de_Storage() != null){
							for(int j = 0; j < to.getProduct_Details().getMaquina_Virtual().get(i).getDados_de_Storage().size(); j++){
								to.getProduct_Details().getMaquina_Virtual().get(i).getDados_de_Storage().get(j).setId(cliente.getProduct().getMaquina_Virtual().get(i).getDados_de_Storage().get(j).getId());
							}
						}
					}
				}
			}
		}
		
		cliente.setClient_Name(to.getClient_Details().getClient_Name());
		cliente.setCPF_CNPJ(to.getClient_Details().getCPF_CNPJ());
		cliente.setCustomer_ID(to.getClient_Details().getCustomer_ID());
		cliente.setDescription(to.getClient_Details().getDescription());
		cliente.setEmail(to.getClient_Details().getEmail());
		cliente.setProduct(to.getProduct_Details());
		cliente.setSTATUS(to.getClient_Details().getSTATUS());
		return cliente;
		
	}

}
