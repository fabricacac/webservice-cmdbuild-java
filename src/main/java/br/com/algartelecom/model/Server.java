package br.com.algartelecom.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.SelectBeforeUpdate;

@Entity
@SelectBeforeUpdate
@SequenceGenerator(name = "nome_seq_server", sequenceName = "class_seq", allocationSize = 1, initialValue = 1)
@Table(name="\"Servidor\"")
public class Server implements Serializable{

	private static final long serialVersionUID = -2293581305344066119L;

	@Id
	@GeneratedValue(generator = "nome_seq_server")
	@Column(name="\"Id\"", unique=true, nullable=false)
	private Integer id;

	@Column(name = "\"Service_ID\"", nullable = true)
	private String Service_ID;

	@Transient
	@JsonIgnore
	private Action action;

	@JsonIgnore
	@Column(name = "\"Description\"", nullable = false)
	private String Description;

	@Column(name = "\"Provisioning_ID\"", nullable = true)
	private String Provisioning_ID;

	@Column(name = "\"Administration_of_Email\"", nullable = true)
	private String Administration_of_Email;

	@Column(name = "\"Administration_of_Exchange\"", nullable = true)
	private String Administration_of_Exchange;

	@Column(name = "\"Administration_of_Proxy\"", nullable = true)
	private String Administration_of_Proxy;

	@Column(name = "\"Administration_OperationalSystem\"", nullable = true)
	private String Administration_OperationalSystem ;

	@Column(name = "\"Amount_of_Cores\"", nullable = true)
	private String Amount_of_Cores;

	@Column(name = "\"Amount_of_HardDisk\"", nullable = true)
	private String Amount_of_HardDisk;

	@Column(name = "\"Amount_of_Memory\"", nullable = true)
	private String Amount_of_Memory;

	@Column(name = "\"Amount_of_Monitoring\"", nullable = true)
	private String Amount_of_Monitoring;

	@Column(name = "\"Amount_of_NetworkBoard\"", nullable = true)
	private String Amount_of_NetworkBoard;

	@Column(name = "\"Amount_of_Processors\"", nullable = true)
	private String Amount_of_Processors;

	@Column(name = "\"Amount_of_SanBoard\"", nullable = true)
	private String Amount_of_SanBoard ;

	@Column(name = "\"Amount_of_SGBDAdministration\"", nullable = true)
	private String Amount_of_SGBDAdministration;

	@Column(name = "\"Amount_of_Storage\"", nullable = true)
	private String Amount_of_Storage;

	@Column(name = "\"Antivirus\"", nullable = true)
	private String Antivirus;

	@Column(name = "\"Function\"", nullable = true)
	private String Function;

	@Column(name = "\"Licensing_ExchangeEnterprisePerUser\"", nullable = true)
	private String Licensing_ExchangeEnterprisePerUser;

	@Column(name = "\"Licensing_ExchangeStandardPerUser\"", nullable = true)
	private String Licensing_ExchangeStandardPerUser;

	@Column(name = "\"Licensing_SQLEnterprisePerCore\"", nullable = true)
	private String Licensing_SQLEnterprisePerCore;

	@Column(name = "\"Licensing_SQLStandardPerCore\"", nullable = true)
	private String Licensing_SQLStandardPerCore;

	@Column(name = "\"Licensing_TerminalService\"", nullable = true)
	private String Licensing_TerminalService;

	@Column(name = "\"Licensing_WindowsDataCenter\"", nullable = true)
	private String Licensing_WindowsDataCenter ;

	@Column(name = "\"Licensing_WindowsStandard\"", nullable = true)
	private String Licensing_WindowsStandard;

	@Column(name = "\"Proprietario\"", nullable = true)
	private String Proprietario;

	@Column(name = "\"Rack_Space\"", nullable = true)
	private String Rack_Space;

	@Column(name = "\"Size_of_Backup\"", nullable = true)
	private String Size_of_Backup;

	@Column(name = "\"Size_of_HardDisk\"", nullable = true)
	private String Size_of_HardDisk  ;

	@Column(name = "\"Fornecedor_Licenca_Antivirus_e_AntiSpam\"", nullable = true)
	private String Fornecedor_Licenca_Antivirus_e_AntiSpam;

	@Column(name = "\"Fornecedor_Licenca_Banco_de_Dados\"", nullable = true)
	private String Fornecedor_Licenca_Banco_de_Dados;

	@Column(name = "\"Fornecedor_Licenca_Office\"", nullable = true)
	private String Fornecedor_Licenca_Office;

	@Column(name = "\"Fornecedor_Licenca_Plataforma_de_Email\"", nullable = true)
	private String Fornecedor_Licenca_Plataforma_de_Email;

	@Column(name = "\"Fornecedor_Licenca_Sharepoint\"", nullable = true)
	private String Fornecedor_Licenca_Sharepoint;

	@Column(name = "\"Fornecedor_Licenca_Sistema_Operacional\"", nullable = true)
	private String Fornecedor_Licenca_Sistema_Operacional;

	@Column(name = "\"Fornecedor_Licenca_Terminal_Service\"", nullable = true)
	private String Fornecedor_Licenca_Terminal_Service;

	@Column(name = "\"Fornecedor_Licenca_Virtualizador\"", nullable = true)
	private String Fornecedor_Licenca_Virtualizador;

	@Column(name = "\"Fornecedor_Licenca_Visual_Studio\"", nullable = true)
	private String Fornecedor_Licenca_Visual_Studio;

	@Column(name = "\"Licenca_Antivirus_e_AntiSpam\"", nullable = true)
	private String Licenca_Antivirus_e_AntiSpam;

	@Column(name = "\"Licenca_Banco_de_Dados\"", nullable = true)
	private String Licenca_Banco_de_Dados;

	@Column(name = "\"Licenca_Office\"", nullable = true)
	private String Licenca_Office;

	@Column(name = "\"Licenca_Plataforma_de_Email\"", nullable = true)
	private String Licenca_Plataforma_de_Email;

	@Column(name = "\"Licenca_Sharepoint\"", nullable = true)
	private String Licenca_Sharepoint;

	@Column(name = "\"Licenca_Sistema_Operacional\"", nullable = true)
	private String Licenca_Sistema_Operacional;

	@Column(name = "\"Licenca_Terminal_Service\"", nullable = true)
	private String Licenca_Terminal_Service;

	@Column(name = "\"Licenca_Virtualizador\"", nullable = true)
	private String Licenca_Virtualizador;

	@Column(name = "\"Licenca_Visual_Studio\"", nullable = true)
	private String Licenca_Visual_Studio;

	@Column(name = "\"Quantidade_Licencas_Antivirus_e_AntiSpam\"", nullable = true)
	private String Quantidade_Licencas_Antivirus_e_AntiSpam;

	@Column(name = "\"Quantidade_Licencas_Banco_de_Dados\"", nullable = true)
	private String Quantidade_Licencas_Banco_de_Dados;

	@Column(name = "\"Quantidade_Licencas_Office\"", nullable = true)
	private String Quantidade_Licencas_Office;

	@Column(name = "\"Quantidade_Licencas_Plataforma_de_Email\"", nullable = true)
	private String Quantidade_Licencas_Plataforma_de_Email;

	@Column(name = "\"Quantidade_Licencas_Sharepoint\"", nullable = true)
	private String Quantidade_Licencas_Sharepoint;

	@Column(name = "\"Quantidade_Licencas_Sistema_Operacional\"", nullable = true)
	private String Quantidade_Licencas_Sistema_Operacional;

	@Column(name = "\"Quantidade_Licencas_Terminal_Service\"", nullable = true)
	private String Quantidade_Licencas_Terminal_Service;

	@Column(name = "\"Quantidade_Licencas_Virtualizador\"", nullable = true)
	private String Quantidade_Licencas_Virtualizador;

	@Column(name = "\"Quantidade_Licencas_Visual_Studio\"", nullable = true)
	private String Quantidade_Licencas_Visual_Studio;

	@Column(name = "\"Administracao_Banco_de_Dados\"", nullable = true)
	private String Administracao_Banco_de_Dados;

	@Column(name = "\"Administracao_Citrix\"", nullable = true)
	private String Administracao_Citrix;

	@Column(name = "\"Administracao_Plataforma_Email\"", nullable = true)
	private String Administracao_Plataforma_Email;

	@Column(name = "\"Administracao_Proxy\"", nullable = true)
	private String Administracao_Proxy;

	@Column(name = "\"Administracao_Sistema_Operacional\"", nullable = true)
	private String Administracao_Sistema_Operacional;

	@Column(name = "\"Administracao_Virtualizador\"", nullable = true)
	private String Administracao_Virtualizador;

	@Column(name = "\"Balanceador_de_Cargas_dedicado_VDOM\"", nullable = true)
	private String Balanceador_de_Cargas_dedicado_VDOM;

	@Column(name = "\"Firewall_Compartilhado_para_BandaIP\"", nullable = true)
	private String Firewall_Compartilhado_para_BandaIP;

	@Column(name = "\"Firewall_Virtual_Dedicado_com_IPS\"", nullable = true)
	private String Firewall_Virtual_Dedicado_com_IPS;

	@Column(name = "\"Monitoramento\"", nullable = true)
	private String Monitoramento;

	@Column(name = "\"Qtd_Admin_Banco_de_Dados\"", nullable = true)
	private String Qtd_Admin_Banco_de_Dados;

	@Column(name = "\"Qtd_Admin_Citrix\"", nullable = true)
	private String Qtd_Admin_Citrix;

	@Column(name = "\"Qtd_Admin_Plataforma_Email\"", nullable = true)
	private String Qtd_Admin_Plataforma_Email;

	@Column(name = "\"Qtd_Admin_Proxy\"", nullable = true)
	private String Qtd_Admin_Proxy;

	@Column(name = "\"Qtd_Admin_Sistema_Operacional\"", nullable = true)
	private String Qtd_Admin_Sistema_Operacional;

	@Column(name = "\"Qtd_Admin_Virtualizador\"", nullable = true)
	private String Qtd_Admin_Virtualizador;

	@Column(name = "\"Qtd_Monitoramento\"", nullable = true)
	private String Qtd_Monitoramento;

	@Column(name = "\"VPN_Client\"", nullable = true)
	private String VPN_Client;

	@Column(name = "\"VPN_Gateway_to_Gateway\"", nullable = true)
	private String VPN_Gateway_to_Gateway;

	@Column(name = "\"Web_application_firewall\"", nullable = true)
	private String Web_application_firewall;

	@Column(name = "\"Numero_de_Serie\"", nullable = true)
	private String Numero_de_Serie;

	@Column(name = "\"Numero_do_patrimonio\"", nullable = true)
	private String Numero_do_patrimonio;

	@Column(name = "\"Fabricante_Modelo_Servidor\"", nullable = true)
	private String Fabricante_Modelo_Servidor;

	@Column(name = "\"Sistema_Operacional\"", nullable = true)
	private String Sistema_Operacional;

	@Column(name = "\"Quantidade_de_processador\"", nullable = true)
	private String Quantidade_de_processador;

	@Column(name = "\"Quantidade_de_Cores\"", nullable = true)
	private String Quantidade_de_Cores;

	@Column(name = "\"Ip_do_Servidor\"", nullable = true)
	private String Ip_do_Servidor;

	@Column(name = "\"Ip_do_Servidor_Back_UP\"", nullable = true)
	private String Ip_do_Servidor_Back_UP;

	@Column(name = "\"Memoria_RAM\"", nullable = true)
	private String Memoria_RAM;

	@Column(name = "\"HBA\"", nullable = true)
	private String HBA;

	@Column(name = "\"Volume_do_Disco_Rigido\"", nullable = true)
	private String Volume_do_Disco_Rigido;

	@Column(name = "\"Tier\"", nullable = true)
	private String Tier;

	@Column(name = "\"Proprietario_do_IC\"", nullable = true)
	private String Proprietario_do_IC;

	@Column(name = "\"Data_Center\"", nullable = true)
	private String Data_Center;

	@Column(name = "\"Andar\"", nullable = true)
	private String Andar;

	@Column(name = "\"Fila\"", nullable = true)
	private String Fila;

	@Column(name = "\"Bastidor\"", nullable = true)
	private String Bastidor;

	@Column(name = "\"Trilho\"", nullable = true)
	private String Trilho;

	@Column(name = "\"Cluster_name\"", nullable = true)
	private String Cluster_name;

	@Column(name = "\"Host_name\"", nullable = true)
	private String Host_name;

	@Column(name = "\"Backup\"", nullable = true)
	private String Backup;

	@Column(name = "\"Quantidade_Backup_GB\"", nullable = true)
	private String Quantidade_Backup_GB;

	@Column(name = "\"Criticidade_do_Backup\"", nullable = true)
	private String Criticidade_do_Backup;

	@Column(name = "\"STATUS\"", nullable = true)
	private String STATUS;

	@ManyToOne
	@JoinColumn(name="\"ID_Service_Group_Instance_ID\"")
	@JsonBackReference
	private Produto produtoServidor;

	@OneToMany(mappedBy = "servidorStorage",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT) 
	@JsonManagedReference
	private List<StorageServer> Dados_de_Storage;


	public String getService_ID() {
		return Service_ID;
	}

	public void setService_ID(String service_ID) {
		Service_ID = service_ID;
	}

	public String getProvisioning_ID() {
		return Provisioning_ID;
	}

	public void setProvisioning_ID(String provisioning_ID) {
		Provisioning_ID = provisioning_ID;
	}

	public String getAdministration_of_Proxy() {
		return Administration_of_Proxy;
	}

	public void setAdministration_of_Proxy(String administration_of_Proxy) {
		Administration_of_Proxy = administration_of_Proxy;
	}

	public String getAdministration_OperationalSystem() {
		return Administration_OperationalSystem;
	}

	public void setAdministration_OperationalSystem(String administration_OperationalSystem) {
		Administration_OperationalSystem = administration_OperationalSystem;
	}

	public String getAmount_of_Cores() {
		return Amount_of_Cores;
	}

	public void setAmount_of_Cores(String amount_of_Cores) {
		Amount_of_Cores = amount_of_Cores;
	}

	public String getAmount_of_HardDisk() {
		return Amount_of_HardDisk;
	}

	public void setAmount_of_HardDisk(String amount_of_HardDisk) {
		Amount_of_HardDisk = amount_of_HardDisk;
	}

	public String getAmount_of_Memory() {
		return Amount_of_Memory;
	}

	public void setAmount_of_Memory(String amount_of_Memory) {
		Amount_of_Memory = amount_of_Memory;
	}

	public String getAmount_of_Monitoring() {
		return Amount_of_Monitoring;
	}

	public void setAmount_of_Monitoring(String amount_of_Monitoring) {
		Amount_of_Monitoring = amount_of_Monitoring;
	}

	public String getAmount_of_NetworkBoard() {
		return Amount_of_NetworkBoard;
	}

	public void setAmount_of_NetworkBoard(String amount_of_NetworkBoard) {
		Amount_of_NetworkBoard = amount_of_NetworkBoard;
	}

	public String getAmount_of_Processors() {
		return Amount_of_Processors;
	}

	public void setAmount_of_Processors(String amount_of_Processors) {
		Amount_of_Processors = amount_of_Processors;
	}

	public String getAmount_of_SanBoard() {
		return Amount_of_SanBoard;
	}

	public void setAmount_of_SanBoard(String amount_of_SanBoard) {
		Amount_of_SanBoard = amount_of_SanBoard;
	}

	public String getAmount_of_SGBDAdministration() {
		return Amount_of_SGBDAdministration;
	}

	public void setAmount_of_SGBDAdministration(String amount_of_SGBDAdministration) {
		Amount_of_SGBDAdministration = amount_of_SGBDAdministration;
	}

	public String getAmount_of_Storage() {
		return Amount_of_Storage;
	}

	public void setAmount_of_Storage(String amount_of_Storage) {
		Amount_of_Storage = amount_of_Storage;
	}

	public String getAntivirus() {
		return Antivirus;
	}

	public void setAntivirus(String antivirus) {
		Antivirus = antivirus;
	}

	public String getFunction() {
		return Function;
	}

	public void setFunction(String function) {
		Function = function;
	}

	public String getLicensing_ExchangeEnterprisePerUser() {
		return Licensing_ExchangeEnterprisePerUser;
	}

	public void setLicensing_ExchangeEnterprisePerUser(String licensing_ExchangeEnterprisePerUser) {
		Licensing_ExchangeEnterprisePerUser = licensing_ExchangeEnterprisePerUser;
	}

	public String getLicensing_ExchangeStandardPerUser() {
		return Licensing_ExchangeStandardPerUser;
	}

	public void setLicensing_ExchangeStandardPerUser(String licensing_ExchangeStandardPerUser) {
		Licensing_ExchangeStandardPerUser = licensing_ExchangeStandardPerUser;
	}

	public String getLicensing_SQLEnterprisePerCore() {
		return Licensing_SQLEnterprisePerCore;
	}

	public void setLicensing_SQLEnterprisePerCore(String licensing_SQLEnterprisePerCore) {
		Licensing_SQLEnterprisePerCore = licensing_SQLEnterprisePerCore;
	}

	public String getLicensing_SQLStandardPerCore() {
		return Licensing_SQLStandardPerCore;
	}

	public void setLicensing_SQLStandardPerCore(String licensing_SQLStandardPerCore) {
		Licensing_SQLStandardPerCore = licensing_SQLStandardPerCore;
	}

	public String getLicensing_TerminalService() {
		return Licensing_TerminalService;
	}

	public void setLicensing_TerminalService(String licensing_TerminalService) {
		Licensing_TerminalService = licensing_TerminalService;
	}

	public String getLicensing_WindowsDataCenter() {
		return Licensing_WindowsDataCenter;
	}

	public void setLicensing_WindowsDataCenter(String licensing_WindowsDataCenter) {
		Licensing_WindowsDataCenter = licensing_WindowsDataCenter;
	}

	public String getLicensing_WindowsStandard() {
		return Licensing_WindowsStandard;
	}

	public void setLicensing_WindowsStandard(String licensing_WindowsStandard) {
		Licensing_WindowsStandard = licensing_WindowsStandard;
	}

	public String getProprietario() {
		return Proprietario;
	}

	public void setProprietario(String proprietario) {
		Proprietario = proprietario;
	}

	public String getRack_Space() {
		return Rack_Space;
	}

	public void setRack_Space(String rack_Space) {
		Rack_Space = rack_Space;
	}

	public String getSize_of_Backup() {
		return Size_of_Backup;
	}

	public void setSize_of_Backup(String size_of_Backup) {
		Size_of_Backup = size_of_Backup;
	}

	public String getSize_of_HardDisk() {
		return Size_of_HardDisk;
	}

	public void setSize_of_HardDisk(String size_of_HardDisk) {
		Size_of_HardDisk = size_of_HardDisk;
	}

	public String getFornecedor_Licenca_Antivirus_e_AntiSpam() {
		return Fornecedor_Licenca_Antivirus_e_AntiSpam;
	}

	public void setFornecedor_Licenca_Antivirus_e_AntiSpam(String fornecedor_Licenca_Antivirus_e_AntiSpam) {
		Fornecedor_Licenca_Antivirus_e_AntiSpam = fornecedor_Licenca_Antivirus_e_AntiSpam;
	}

	public String getFornecedor_Licenca_Banco_de_Dados() {
		return Fornecedor_Licenca_Banco_de_Dados;
	}

	public void setFornecedor_Licenca_Banco_de_Dados(String fornecedor_Licenca_Banco_de_Dados) {
		Fornecedor_Licenca_Banco_de_Dados = fornecedor_Licenca_Banco_de_Dados;
	}

	public String getFornecedor_Licenca_Office() {
		return Fornecedor_Licenca_Office;
	}

	public void setFornecedor_Licenca_Office(String fornecedor_Licenca_Office) {
		Fornecedor_Licenca_Office = fornecedor_Licenca_Office;
	}

	public String getFornecedor_Licenca_Plataforma_de_Email() {
		return Fornecedor_Licenca_Plataforma_de_Email;
	}

	public void setFornecedor_Licenca_Plataforma_de_Email(String fornecedor_Licenca_Plataforma_de_Email) {
		Fornecedor_Licenca_Plataforma_de_Email = fornecedor_Licenca_Plataforma_de_Email;
	}

	public String getFornecedor_Licenca_Sharepoint() {
		return Fornecedor_Licenca_Sharepoint;
	}

	public void setFornecedor_Licenca_Sharepoint(String fornecedor_Licenca_Sharepoint) {
		Fornecedor_Licenca_Sharepoint = fornecedor_Licenca_Sharepoint;
	}

	public String getFornecedor_Licenca_Sistema_Operacional() {
		return Fornecedor_Licenca_Sistema_Operacional;
	}

	public void setFornecedor_Licenca_Sistema_Operacional(String fornecedor_Licenca_Sistema_Operacional) {
		Fornecedor_Licenca_Sistema_Operacional = fornecedor_Licenca_Sistema_Operacional;
	}

	public String getFornecedor_Licenca_Terminal_Service() {
		return Fornecedor_Licenca_Terminal_Service;
	}

	public void setFornecedor_Licenca_Terminal_Service(String fornecedor_Licenca_Terminal_Service) {
		Fornecedor_Licenca_Terminal_Service = fornecedor_Licenca_Terminal_Service;
	}

	public String getFornecedor_Licenca_Virtualizador() {
		return Fornecedor_Licenca_Virtualizador;
	}

	public void setFornecedor_Licenca_Virtualizador(String fornecedor_Licenca_Virtualizador) {
		Fornecedor_Licenca_Virtualizador = fornecedor_Licenca_Virtualizador;
	}

	public String getFornecedor_Licenca_Visual_Studio() {
		return Fornecedor_Licenca_Visual_Studio;
	}

	public void setFornecedor_Licenca_Visual_Studio(String fornecedor_Licenca_Visual_Studio) {
		Fornecedor_Licenca_Visual_Studio = fornecedor_Licenca_Visual_Studio;
	}

	public String getLicenca_Antivirus_e_AntiSpam() {
		return Licenca_Antivirus_e_AntiSpam;
	}

	public void setLicenca_Antivirus_e_AntiSpam(String licenca_Antivirus_e_AntiSpam) {
		Licenca_Antivirus_e_AntiSpam = licenca_Antivirus_e_AntiSpam;
	}

	public String getLicenca_Banco_de_Dados() {
		return Licenca_Banco_de_Dados;
	}

	public void setLicenca_Banco_de_Dados(String licenca_Banco_de_Dados) {
		Licenca_Banco_de_Dados = licenca_Banco_de_Dados;
	}

	public String getLicenca_Office() {
		return Licenca_Office;
	}

	public void setLicenca_Office(String licenca_Office) {
		Licenca_Office = licenca_Office;
	}

	public String getLicenca_Plataforma_de_Email() {
		return Licenca_Plataforma_de_Email;
	}

	public void setLicenca_Plataforma_de_Email(String licenca_Plataforma_de_Email) {
		Licenca_Plataforma_de_Email = licenca_Plataforma_de_Email;
	}

	public String getLicenca_Sharepoint() {
		return Licenca_Sharepoint;
	}

	public void setLicenca_Sharepoint(String licenca_Sharepoint) {
		Licenca_Sharepoint = licenca_Sharepoint;
	}

	public String getLicenca_Sistema_Operacional() {
		return Licenca_Sistema_Operacional;
	}

	public void setLicenca_Sistema_Operacional(String licenca_Sistema_Operacional) {
		Licenca_Sistema_Operacional = licenca_Sistema_Operacional;
	}

	public String getLicenca_Terminal_Service() {
		return Licenca_Terminal_Service;
	}

	public void setLicenca_Terminal_Service(String licenca_Terminal_Service) {
		Licenca_Terminal_Service = licenca_Terminal_Service;
	}

	public String getLicenca_Virtualizador() {
		return Licenca_Virtualizador;
	}

	public void setLicenca_Virtualizador(String licenca_Virtualizador) {
		Licenca_Virtualizador = licenca_Virtualizador;
	}

	public String getLicenca_Visual_Studio() {
		return Licenca_Visual_Studio;
	}

	public void setLicenca_Visual_Studio(String licenca_Visual_Studio) {
		Licenca_Visual_Studio = licenca_Visual_Studio;
	}

	public String getQuantidade_Licencas_Antivirus_e_AntiSpam() {
		return Quantidade_Licencas_Antivirus_e_AntiSpam;
	}

	public void setQuantidade_Licencas_Antivirus_e_AntiSpam(String quantidade_Licencas_Antivirus_e_AntiSpam) {
		Quantidade_Licencas_Antivirus_e_AntiSpam = quantidade_Licencas_Antivirus_e_AntiSpam;
	}

	public String getQuantidade_Licencas_Banco_de_Dados() {
		return Quantidade_Licencas_Banco_de_Dados;
	}

	public void setQuantidade_Licencas_Banco_de_Dados(String quantidade_Licencas_Banco_de_Dados) {
		Quantidade_Licencas_Banco_de_Dados = quantidade_Licencas_Banco_de_Dados;
	}

	public String getQuantidade_Licencas_Office() {
		return Quantidade_Licencas_Office;
	}

	public void setQuantidade_Licencas_Office(String quantidade_Licencas_Office) {
		Quantidade_Licencas_Office = quantidade_Licencas_Office;
	}

	public String getQuantidade_Licencas_Plataforma_de_Email() {
		return Quantidade_Licencas_Plataforma_de_Email;
	}

	public void setQuantidade_Licencas_Plataforma_de_Email(String quantidade_Licencas_Plataforma_de_Email) {
		Quantidade_Licencas_Plataforma_de_Email = quantidade_Licencas_Plataforma_de_Email;
	}

	public String getQuantidade_Licencas_Sharepoint() {
		return Quantidade_Licencas_Sharepoint;
	}

	public void setQuantidade_Licencas_Sharepoint(String quantidade_Licencas_Sharepoint) {
		Quantidade_Licencas_Sharepoint = quantidade_Licencas_Sharepoint;
	}

	public String getQuantidade_Licencas_Sistema_Operacional() {
		return Quantidade_Licencas_Sistema_Operacional;
	}

	public void setQuantidade_Licencas_Sistema_Operacional(String quantidade_Licencas_Sistema_Operacional) {
		Quantidade_Licencas_Sistema_Operacional = quantidade_Licencas_Sistema_Operacional;
	}

	public String getQuantidade_Licencas_Terminal_Service() {
		return Quantidade_Licencas_Terminal_Service;
	}

	public void setQuantidade_Licencas_Terminal_Service(String quantidade_Licencas_Terminal_Service) {
		Quantidade_Licencas_Terminal_Service = quantidade_Licencas_Terminal_Service;
	}

	public String getQuantidade_Licencas_Virtualizador() {
		return Quantidade_Licencas_Virtualizador;
	}

	public void setQuantidade_Licencas_Virtualizador(String quantidade_Licencas_Virtualizador) {
		Quantidade_Licencas_Virtualizador = quantidade_Licencas_Virtualizador;
	}

	public String getQuantidade_Licencas_Visual_Studio() {
		return Quantidade_Licencas_Visual_Studio;
	}

	public void setQuantidade_Licencas_Visual_Studio(String quantidade_Licencas_Visual_Studio) {
		Quantidade_Licencas_Visual_Studio = quantidade_Licencas_Visual_Studio;
	}

	public String getAdministracao_Banco_de_Dados() {
		return Administracao_Banco_de_Dados;
	}

	public void setAdministracao_Banco_de_Dados(String administracao_Banco_de_Dados) {
		Administracao_Banco_de_Dados = administracao_Banco_de_Dados;
	}

	public String getAdministracao_Citrix() {
		return Administracao_Citrix;
	}

	public void setAdministracao_Citrix(String administracao_Citrix) {
		Administracao_Citrix = administracao_Citrix;
	}

	public String getAdministracao_Plataforma_Email() {
		return Administracao_Plataforma_Email;
	}

	public void setAdministracao_Plataforma_Email(String administracao_Plataforma_Email) {
		Administracao_Plataforma_Email = administracao_Plataforma_Email;
	}

	public String getAdministracao_Proxy() {
		return Administracao_Proxy;
	}

	public void setAdministracao_Proxy(String administracao_Proxy) {
		Administracao_Proxy = administracao_Proxy;
	}

	public String getAdministracao_Sistema_Operacional() {
		return Administracao_Sistema_Operacional;
	}

	public void setAdministracao_Sistema_Operacional(String administracao_Sistema_Operacional) {
		Administracao_Sistema_Operacional = administracao_Sistema_Operacional;
	}

	public String getAdministracao_Virtualizador() {
		return Administracao_Virtualizador;
	}

	public void setAdministracao_Virtualizador(String administracao_Virtualizador) {
		Administracao_Virtualizador = administracao_Virtualizador;
	}

	public String getBalanceador_de_Cargas_dedicado_VDOM() {
		return Balanceador_de_Cargas_dedicado_VDOM;
	}

	public void setBalanceador_de_Cargas_dedicado_VDOM(String balanceador_de_Cargas_) {
		Balanceador_de_Cargas_dedicado_VDOM = balanceador_de_Cargas_;
	}

	public String getFirewall_Compartilhado_para_BandaIP() {
		return Firewall_Compartilhado_para_BandaIP;
	}

	public void setFirewall_Compartilhado_para_BandaIP(String firewall_Compartilhado_para_BandaIP) {
		Firewall_Compartilhado_para_BandaIP = firewall_Compartilhado_para_BandaIP;
	}

	public String getFirewall_Virtual_Dedicado_com_IPS() {
		return Firewall_Virtual_Dedicado_com_IPS;
	}

	public void setFirewall_Virtual_Dedicado_com_IPS(String firewall_Virtual_Dedicado_com_IPS) {
		Firewall_Virtual_Dedicado_com_IPS = firewall_Virtual_Dedicado_com_IPS;
	}

	public String getMonitoramento() {
		return Monitoramento;
	}

	public void setMonitoramento(String monitoramento) {
		Monitoramento = monitoramento;
	}

	public String getQtd_Admin_Banco_de_Dados() {
		return Qtd_Admin_Banco_de_Dados;
	}

	public void setQtd_Admin_Banco_de_Dados(String qtd_Admin_Banco_de_Dados) {
		Qtd_Admin_Banco_de_Dados = qtd_Admin_Banco_de_Dados;
	}

	public String getQtd_Admin_Citrix() {
		return Qtd_Admin_Citrix;
	}

	public void setQtd_Admin_Citrix(String qtd_Admin_Citrix) {
		Qtd_Admin_Citrix = qtd_Admin_Citrix;
	}

	public String getQtd_Admin_Plataforma_Email() {
		return Qtd_Admin_Plataforma_Email;
	}

	public void setQtd_Admin_Plataforma_Email(String qtd_Admin_Plataforma_Email) {
		Qtd_Admin_Plataforma_Email = qtd_Admin_Plataforma_Email;
	}

	public String getQtd_Admin_Proxy() {
		return Qtd_Admin_Proxy;
	}

	public void setQtd_Admin_Proxy(String qtd_Admin_Proxy) {
		Qtd_Admin_Proxy = qtd_Admin_Proxy;
	}

	public String getQtd_Admin_Sistema_Operacional() {
		return Qtd_Admin_Sistema_Operacional;
	}

	public void setQtd_Admin_Sistema_Operacional(String qtd_Admin_Sistema_Operacional) {
		Qtd_Admin_Sistema_Operacional = qtd_Admin_Sistema_Operacional;
	}

	public String getQtd_Admin_Virtualizador() {
		return Qtd_Admin_Virtualizador;
	}

	public void setQtd_Admin_Virtualizador(String qtd_Admin_Virtualizador) {
		Qtd_Admin_Virtualizador = qtd_Admin_Virtualizador;
	}

	public String getQtd_Monitoramento() {
		return Qtd_Monitoramento;
	}

	public void setQtd_Monitoramento(String qtd_Monitoramento) {
		Qtd_Monitoramento = qtd_Monitoramento;
	}

	public String getVPN_Client() {
		return VPN_Client;
	}

	public void setVPN_Client(String VPN_Client) {
		this.VPN_Client = VPN_Client;
	}

	public String getVPN_Gateway_to_Gateway() {
		return VPN_Gateway_to_Gateway;
	}

	public void setVPN_Gateway_to_Gateway(String VPN_Gateway_to_Gateway) {
		this.VPN_Gateway_to_Gateway = VPN_Gateway_to_Gateway;
	}

	public String getWeb_application_firewall() {
		return Web_application_firewall;
	}

	public void setWeb_application_firewall(String web_application_firewall) {
		Web_application_firewall = web_application_firewall;
	}

	public String getNumero_de_Serie() {
		return Numero_de_Serie;
	}

	public void setNumero_de_Serie(String numero_de_Serie) {
		Numero_de_Serie = numero_de_Serie;
	}

	public String getNumero_do_patrimonio() {
		return Numero_do_patrimonio;
	}

	public void setNumero_do_patrimonio(String numero_do_patrimonio) {
		Numero_do_patrimonio = numero_do_patrimonio;
	}

	public String getFabricante_Modelo_Servidor() {
		return Fabricante_Modelo_Servidor;
	}

	public void setFabricante_Modelo_Servidor(String fabricante_Modelo_Servidor) {
		Fabricante_Modelo_Servidor = fabricante_Modelo_Servidor;
	}

	public String getSistema_Operacional() {
		return Sistema_Operacional;
	}

	public void setSistema_Operacional(String sistema_Operacional) {
		Sistema_Operacional = sistema_Operacional;
	}

	public String getQuantidade_de_processador() {
		return Quantidade_de_processador;
	}

	public void setQuantidade_de_processador(String quantidade_de_processador) {
		Quantidade_de_processador = quantidade_de_processador;
	}

	public String getQuantidade_de_Cores() {
		return Quantidade_de_Cores;
	}

	public void setQuantidade_de_Cores(String quantidade_de_Cores) {
		Quantidade_de_Cores = quantidade_de_Cores;
	}

	public String getIp_do_Servidor() {
		return Ip_do_Servidor;
	}

	public void setIp_do_Servidor(String ip_do_Servidor) {
		Ip_do_Servidor = ip_do_Servidor;
	}

	public String getIp_do_Servidor_Back_UP() {
		return Ip_do_Servidor_Back_UP;
	}

	public void setIp_do_Servidor_Back_UP(String ip_do_Servidor_Back_UP) {
		Ip_do_Servidor_Back_UP = ip_do_Servidor_Back_UP;
	}

	public String getMemoria_RAM() {
		return Memoria_RAM;
	}

	public void setMemoria_RAM(String memoria_RAM) {
		Memoria_RAM = memoria_RAM;
	}

	public String getHBA() {
		return HBA;
	}

	public void setHBA(String HBA) {
		this.HBA = HBA;
	}

	public String getVolume_do_Disco_Rigido() {
		return Volume_do_Disco_Rigido;
	}

	public void setVolume_do_Disco_Rigido(String volume_do_Disco_Rigido) {
		Volume_do_Disco_Rigido = volume_do_Disco_Rigido;
	}

	public String getTier() {
		return Tier;
	}

	public void setTier(String tier) {
		Tier = tier;
	}

	public String getProprietario_do_IC() {
		return Proprietario_do_IC;
	}

	public void setProprietario_do_IC(String proprietario_do_IC) {
		Proprietario_do_IC = proprietario_do_IC;
	}

	public String getData_Center() {
		return Data_Center;
	}

	public void setData_Center(String data_Center) {
		Data_Center = data_Center;
	}

	public String getAndar() {
		return Andar;
	}

	public void setAndar(String andar) {
		Andar = andar;
	}

	public String getFila() {
		return Fila;
	}

	public void setFila(String fila) {
		Fila = fila;
	}

	public String getBastidor() {
		return Bastidor;
	}

	public void setBastidor(String bastidor) {
		Bastidor = bastidor;
	}

	public String getTrilho() {
		return Trilho;
	}

	public void setTrilho(String trilho) {
		Trilho = trilho;
	}

	public String getCluster_name() {
		return Cluster_name;
	}

	public void setCluster_name(String cluster_name) {
		Cluster_name = cluster_name;
	}

	public String getHost_name() {
		return Host_name;
	}

	public void setHost_name(String host_name) {
		Host_name = host_name;
	}

	public String getBackup() {
		return Backup;
	}

	public void setBackup(String backup) {
		Backup = backup;
	}

	public String getQuantidade_Backup_GB() {
		return Quantidade_Backup_GB;
	}

	public void setQuantidade_Backup_GB(String quantidade_Backup_GB) {
		Quantidade_Backup_GB = quantidade_Backup_GB;
	}

	public String getCriticidade_do_Backup() {
		return Criticidade_do_Backup;
	}

	public void setCriticidade_do_Backup(String criticidade_do_Backup) {
		Criticidade_do_Backup = criticidade_do_Backup;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String STATUS) {
		this.STATUS = STATUS;
	}

	public String getAdministration_of_Email() {
		return Administration_of_Email;
	}

	public void setAdministration_of_Email(String administration_of_Email) {
		Administration_of_Email = administration_of_Email;
	}

	public String getAdministration_of_Exchange() {
		return Administration_of_Exchange;
	}

	public void setAdministration_of_Exchange(String administration_of_Exchange) {
		Administration_of_Exchange = administration_of_Exchange;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Action getAction() {
		return action;
	}


	public void setAction(Action action) {
		this.action = action;
	}


	public Produto getProdutoServidor() {
		return produtoServidor;
	}


	public void setProdutoServidor(Produto produtoServidor) {
		this.produtoServidor = produtoServidor;
	}


	public List<StorageServer> getDados_de_Storage() {
		return Dados_de_Storage;
	}


	public void setDados_de_Storage(List<StorageServer> dados_de_Storage) {
		Dados_de_Storage = dados_de_Storage;
	}

	public static List<Server> getServerObject(ClienteTO to){
		if(to.getProduct_Details().getServidor() != null && !to.getProduct_Details().getServidor().isEmpty()){
			return to.getProduct_Details().getServidor();
		}
		return null;
	}

	public String getDescription() {
		return Description;
	}


	public void setDescription(String description) {
		Description = description;
	}

}
