package br.com.algartelecom.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.hibernate.annotations.SelectBeforeUpdate;

@Entity
@SelectBeforeUpdate
@SequenceGenerator(name = "nome_seq_storage_virtual_machine", sequenceName = "class_seq", allocationSize = 1, initialValue = 1)
@Table(name="\"Storage_Maquina_Virtual\"")
public class StorageVirtualMachine implements Serializable{

	private static final long serialVersionUID = -7138163433167458657L;

	@Id
	@GeneratedValue(generator="nome_seq_storage_virtual_machine")
	@Column(name="\"Id\"", unique=true, nullable=false)
	private Integer id;

	@Column(name = "\"Description\"", nullable = true)
	private String Nome_da_LUN;

	@Column(name = "\"Quantidade_de_Storage\"", nullable = true)
	private String Quantidade_de_Storage;

	@Column(name = "\"STATUS\"", nullable = true)
	private String STATUS;

	@ManyToOne
	@JoinColumn(name="\"ID_Maquina_Virtual\"")
	@JsonBackReference
	private VirtualMachine maquinaVirtualStorage;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome_da_LUN() {
		return Nome_da_LUN;
	}

	public void setNome_da_LUN(String nome_da_LUN) {
		Nome_da_LUN = nome_da_LUN;
	}

	public String getQuantidade_de_Storage() {
		return Quantidade_de_Storage;
	}

	public void setQuantidade_de_Storage(String quantidade_de_Storage) {
		Quantidade_de_Storage = quantidade_de_Storage;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public VirtualMachine getMaquinaVirtualStorage() {
		return maquinaVirtualStorage;
	}

	public void setMaquinaVirtualStorage(VirtualMachine maquinaVirtualStorage) {
		this.maquinaVirtualStorage = maquinaVirtualStorage;
	}

	public static List<StorageVirtualMachine> getObjectStorageVirtualMachine(ClienteTO to){
		if(to.getProduct_Details() != null && to.getProduct_Details().getMaquina_Virtual() != null){
			for(VirtualMachine v: to.getProduct_Details().getMaquina_Virtual()){
				return v.getDados_de_Storage();
			}
		}
		return null;
	}
}
