package br.com.algartelecom.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.SelectBeforeUpdate;

@Entity
@SelectBeforeUpdate
@SequenceGenerator(name = "nome_seq_produto", sequenceName = "class_seq", allocationSize = 1, initialValue = 1)
@Table(name="\"Produto\"")
public class Produto implements Serializable {

	private static final long serialVersionUID = 6840160684692489394L;

	@Id
	@GeneratedValue(generator="nome_seq_produto")
	@Column(name="\"Id\"", unique=true, nullable=false)
	private Integer id;

	@Column(name = "\"Service_Group_Instance_ID\"", nullable = false)
	private String Service_Group_Instance_ID;

	@Column(name = "\"Network_ID\"", nullable = true)
	private String Network_ID;

	@Column(name = "\"Tipo_Hospedagem\"", nullable = true)
	private String Tipo_Hospedagem;

	@Column(name = "\"Fornecedor_Backup\"", nullable = true)
	private String Fornecedor_Backup;

	@JsonIgnore
	@Column(name = "\"Description\"", nullable = false)
	private String Description;

	@Column(name = "\"Fornecedor_Storage\"", nullable = true)
	private String Fornecedor_Storage;

	@Column(name = "\"Porta_10G\"", nullable = true)
	private String Porta_10G;

	@Column(name = "\"Portas_SAN\"", nullable = true)
	private String Portas_SAN;

	@Column(name = "\"Tipo_Backup\"", nullable = true)
	private String Tipo_Backup;

	@Column(name = "\"Tipo_Storage\"", nullable = true)
	private String Tipo_Storage;

	@Column(name = "\"Quantidade_de_Storage\"", nullable = true)
	private String Quantidade_de_Storage;

	@Column(name = "\"Volume_de_Backup\"", nullable = true)
	private String Volume_de_Backup;

	@Column(name = "\"STATUS\"", nullable = true)
	private String STATUS;

	@OneToMany(mappedBy = "produtoServidor", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	@JsonManagedReference
	private List<Server> servidor; 

	@OneToMany(mappedBy = "produtoMaquinaVirtual", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	@JsonManagedReference
	private List<VirtualMachine> Maquina_Virtual; 

	@OneToOne
	@JoinColumn(name="\"ID_Cliente\"")
	@JsonBackReference
	private Cliente clienteProduto;


	public String getService_Group_Instance_ID() {
		return Service_Group_Instance_ID;
	}

	public void setService_Group_Instance_ID(String service_Group_Instance_ID) {
		Service_Group_Instance_ID = service_Group_Instance_ID;
	}

	public String getNetwork_ID() {
		return Network_ID;
	}

	public void setNetwork_ID(String network_ID) {
		Network_ID = network_ID;
	}

	public String getTipo_Hospedagem() {
		return Tipo_Hospedagem;
	}

	public void setTipo_Hospedagem(String tipo_Hospedagem) {
		Tipo_Hospedagem = tipo_Hospedagem;
	}

	public String getFornecedor_Backup() {
		return Fornecedor_Backup;
	}

	public void setFornecedor_Backup(String fornecedor_Backup) {
		Fornecedor_Backup = fornecedor_Backup;
	}

	public String getFornecedor_Storage() {
		return Fornecedor_Storage;
	}

	public void setFornecedor_Storage(String fornecedor_Storage) {
		Fornecedor_Storage = fornecedor_Storage;
	}

	public String getPorta_10G() {
		return Porta_10G;
	}

	public void setPorta_10G(String porta_10G) {
		Porta_10G = porta_10G;
	}

	public String getPortas_SAN() {
		return Portas_SAN;
	}

	public void setPortas_SAN(String portas_SAN) {
		Portas_SAN = portas_SAN;
	}

	public String getTipo_Backup() {
		return Tipo_Backup;
	}

	public void setTipo_Backup(String tipo_Backup) {
		Tipo_Backup = tipo_Backup;
	}

	public String getTipo_Storage() {
		return Tipo_Storage;
	}

	public void setTipo_Storage(String tipo_Storage) {
		Tipo_Storage = tipo_Storage;
	}

	public String getQuantidade_de_Storage() {
		return Quantidade_de_Storage;
	}

	public void setQuantidade_de_Storage(String quantidade_de_Storage) {
		Quantidade_de_Storage = quantidade_de_Storage;
	}

	public String getVolume_de_Backup() {
		return Volume_de_Backup;
	}

	public void setVolume_de_Backup(String volume_de_Backup) {
		Volume_de_Backup = volume_de_Backup;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String STATUS) {
		this.STATUS = STATUS;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Server> getServidor() {
		return servidor;
	}

	public void setServidor(List<Server> servidor) {
		this.servidor = servidor;
	}

	public List<VirtualMachine> getMaquina_Virtual() {
		return Maquina_Virtual;
	}

	public void setMaquina_Virtual(List<VirtualMachine> maquina_Virtual) {
		Maquina_Virtual = maquina_Virtual;
	}

	public Cliente getClienteProduto() {
		return clienteProduto;
	}

	public void setClienteProduto(Cliente clienteProduto) {
		this.clienteProduto = clienteProduto;
	}

	public static Produto getObjectProduto(ClienteTO to){
		if(to.getProduct_Details() != null){
			to.getProduct_Details().setServidor(null);
			to.getProduct_Details().setMaquina_Virtual(null);
		}
		return to.getProduct_Details();
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;	
	}

}

