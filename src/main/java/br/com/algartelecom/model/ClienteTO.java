package br.com.algartelecom.model;

public class ClienteTO {
	
	private OrderType order_Type;

	private Cliente client_Details;

	private Produto product_Details;

	public Cliente getClient_Details() {
		return client_Details;
	}

	public void setClient_Details(Cliente client_Details) {
		this.client_Details = client_Details;
	}

	public OrderType getOrder_Type() {
		return order_Type;
	}

	public void setOrder_Type(OrderType order_Type) {
		this.order_Type = order_Type;
	}

	public void setProduct_Details(Produto product_Details) {
		this.product_Details = product_Details;
	}

	public Produto getProduct_Details() {
		return product_Details;
	}

}
