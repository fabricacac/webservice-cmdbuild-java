package br.com.algartelecom.dao.client;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.algartelecom.model.Cliente;
import br.com.algartelecom.model.Produto;
import br.com.algartelecom.model.Server;
import br.com.algartelecom.model.StorageServer;
import br.com.algartelecom.model.StorageVirtualMachine;
import br.com.algartelecom.model.VirtualMachine;
import br.com.algartelecom.utils.CRUDResponse;
import br.com.algartelecom.utils.ErroDetails;
import br.com.algartelecom.utils.InternalErrorException;
import br.com.algartelecom.utils.dao.DAO;

@Repository
public class ClientDAO extends DAO{

	@PersistenceContext(unitName="CMDBPU")
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<Cliente> getAllClients(Integer maxResults){
		Query query = entityManager.createQuery("FROM Cliente",Cliente.class);
		if(maxResults != null){
			query.setMaxResults(maxResults);
		}
		return query.getResultList();	
	}

	public Produto getProductGroupID(String serviceGroup){
		try {
			Query query = entityManager.createQuery("FROM Produto WHERE Service_Group_Instance_ID=:group",Produto.class);
			query.setParameter("group", serviceGroup);
			return (Produto) query.getSingleResult();
		} catch (NonUniqueResultException e) {
			throw new InternalErrorException("Service Group exists in more than one product. Service Group: " + serviceGroup);
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public Cliente getClientByDescription(String cpfCNPJ){
		try {
			Query query = entityManager.createQuery("FROM Cliente WHERE Description=:group",Cliente.class);
			query.setParameter("group", cpfCNPJ);
			return (Cliente) query.getSingleResult();
		} catch (NonUniqueResultException e) {
			return null;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public CRUDResponse getClientByDocument(String cpfCNPJ){
		CRUDResponse crud = new CRUDResponse();
		try {
			Query query = entityManager.createQuery("FROM Cliente WHERE Description=:group",Cliente.class);
			query.setParameter("group", cpfCNPJ);
			Cliente c =  (Cliente) query.getSingleResult();
			if(c != null){
				crud.setCliente(c);
				return crud;
			}
		} catch (NonUniqueResultException e) {
			crud.setStatus(1);
			crud.setErro_Deatils(ErroDetails.setObjectDetails("500", "No unique result for client. Document: " + cpfCNPJ));
		} catch (NoResultException e) {
			crud.setStatus(1);
			crud.setErro_Deatils(ErroDetails.setObjectDetails("404", "Client not found. Document: " + cpfCNPJ));
		}
		return crud;
	}
	
	public CRUDResponse getClientByInsert(String cpfCNPJ){
		CRUDResponse crud = new CRUDResponse();
		try {
			Query query = entityManager.createQuery("FROM Cliente WHERE Description=:group",Cliente.class);
			query.setParameter("group", cpfCNPJ);
			Cliente c =  (Cliente) query.getSingleResult();
			if(c != null){
				crud.setCliente(c);
				return crud;
			}
		} catch (NonUniqueResultException e) {
			crud.setStatus(1);
			crud.setErro_Deatils(ErroDetails.setObjectDetails("500", "No unique result for client. Document: " + cpfCNPJ));
		} catch (NoResultException e) {
			return null;
		}
		return crud;
	}
	
	public CRUDResponse getProductByDocument(String groupId){
		CRUDResponse crud = new CRUDResponse();
		try {
			Query query = entityManager.createQuery("FROM Produto WHERE Service_Group_Instance_ID=:group",Cliente.class);
			query.setParameter("group", groupId);
			Cliente c =  (Cliente) query.getSingleResult();
			if(c != null){
				crud.setCliente(c);
				return crud;
			}
		} catch (NonUniqueResultException e) {
			crud.setStatus(1);
			crud.setErro_Deatils(ErroDetails.setObjectDetails("500", "No unique result for product. Document: " + groupId));
		} catch (NoResultException e) {
			crud.setStatus(1);
			crud.setErro_Deatils(ErroDetails.setObjectDetails("404", "Product not found. Service_Group_Instance_ID: " + groupId));
		}
		return crud;
	}

	@Transactional
	public void saveClient(Cliente client){
		entityManager.persist(client);
	}

	@Transactional
	public void saveServer(Server client){
		entityManager.persist(client);
	}
	
	@Transactional
	public void saveServerStorage(StorageServer storageServidor){
		entityManager.persist(storageServidor);
	}

	@Transactional
	public void saveVirtualMachine(VirtualMachine client){
		entityManager.persist(client);
	}

	@Transactional
	public void saveStorage(StorageServer client){
		entityManager.persist(client);
	}
	
	@Transactional
	public void saveStorageVirtualMachine(StorageVirtualMachine client){
		entityManager.persist(client);
	}
	
	@Transactional
	public Produto saveProduto(Produto produto){
		entityManager.persist(produto);
		return produto;
	}

	@Transactional
	public void updateClient(Cliente client){
		entityManager.merge(client);
	}
	
	@Transactional
	public void updateProduto(Produto client){
		entityManager.merge(client);
	}



}
