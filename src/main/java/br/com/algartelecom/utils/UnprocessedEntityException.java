package br.com.algartelecom.utils;

import java.util.List;

public class UnprocessedEntityException extends RuntimeException {

	private static final long serialVersionUID = -981053780941770546L;
	
	private final List<Object> data;
	
	public UnprocessedEntityException(String msg, List<Object> data) {
		super(msg);
		this.data = data;
	}

	public List<Object> getData() {
		return data;
	}

}
