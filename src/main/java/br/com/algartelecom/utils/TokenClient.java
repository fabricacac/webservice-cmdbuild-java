package br.com.algartelecom.utils;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

import br.com.algartelecom.model.AuthenticatedTO;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@Component
public class TokenClient {

	@Autowired
	private ConfigCMDBuild config;

	@Autowired
	private RestClientURL restClient;

	private AuthenticatedTO auth;

	@PostConstruct
	public AuthenticatedTO getToken(){
		ClientResponse response = null;

		try {
			LoggingFilter logging = new LoggingFilter(Logger.getAnonymousLogger());

			JSONObject jsonInput = new JSONObject()   
					.element( "username", config.getUsername() )  
					.element( "password", config.getPassword() );

			WebResource service = restClient.getClient().resource(UriBuilder.fromUri(restClient.getUrl()+ConstantsURL.createSession).build());
			service.addFilter(logging);
			service.addFilter(new RedirectFilter());

			response = restClient.post(service, jsonInput.toString());

			JSONObject jsonOutput = (JSONObject) JSONSerializer.toJSON( response.getEntity(String.class)); 
			this.auth = AuthenticatedTO.setReturnObject(jsonOutput);
			return this.auth;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return auth;
	}

	public String readSession(){
		WebResource service = restClient.getClient().resource(UriBuilder.fromUri(restClient.getUrl()+ConstantsURL.verifySession + this.auth.get_id()).build());
		ClientResponse response = service.type(MediaType.APPLICATION_JSON)
				.header("CMDBuild-Authorization", this.auth.get_id())
				.get(ClientResponse.class);
		try {
			if(response.getStatus() != 200){
				this.deleteSession();
				this.getToken();
			}else{
				JSONObject jsonOutput = (JSONObject) JSONSerializer.toJSON( response.getEntity(String.class));
				this.auth = AuthenticatedTO.setReturnObject(jsonOutput);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.auth.get_id();
	}

	public void deleteSession(){
		try {
			WebResource service = restClient.getClient().resource(UriBuilder.fromUri(restClient.getUrl()+ConstantsURL.deleteSession + this.auth.get_id()).build());
			ClientResponse response = service.type(MediaType.APPLICATION_JSON)
					.header("CMDBuild-Authorization", this.auth.get_id())
					.delete(ClientResponse.class);
			if(response.getStatus() != 204){
				throw new InternalErrorException("Session not delete");
			}
		} catch (Exception e) {
		}

	}

	@PreDestroy
	public void cleanUp() throws Exception {
		System.out.println("Spring Container is destroy! Customer clean up");
	}

	public AuthenticatedTO getAuth() {
		return auth;
	}

	public void setAuth(AuthenticatedTO auth) {
		this.auth = auth;
	}

}
