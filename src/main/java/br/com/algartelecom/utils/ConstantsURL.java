package br.com.algartelecom.utils;

public final class ConstantsURL {
	
	private ConstantsURL(){};
	
	public static final String createSession = "/cmdbuild/services/rest/v2/sessions";
	public static final String verifySession = "/cmdbuild/services/rest/v2/sessions/";
	public static final String deleteSession = "/cmdbuild/services/rest/v2/sessions/";
	public static final String getAllClasses = "/cmdbuild/services/rest/v2/classes";
	public static final String readDetailsClass = "/cmdbuild/services/rest/v2/classes/%s";
	public static final String readAttributesClass = "/cmdbuild/services/rest/v2/classes/%s/attributes";
	public static final String createCard = "/cmdbuild/services/rest/v2/classes/%s/cards";
	public static final String listAllCards = "/cmdbuild/services/rest/v2/classes/%s/cards";
	public static final String readingDetailsCard = "/cmdbuild/services/rest/v2/classes/%s/cards/%s";
	public static final String updateCard = "/cmdbuild/services/rest/v2/classes/%s/cards/%s";
	
	

}
