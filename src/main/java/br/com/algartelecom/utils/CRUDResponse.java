package br.com.algartelecom.utils;

import org.codehaus.jackson.annotate.JsonIgnore;

import br.com.algartelecom.model.Cliente;
import br.com.algartelecom.model.Produto;

public class CRUDResponse {
	
	@JsonIgnore
	private Cliente cliente;
	
	@JsonIgnore
	private Produto produto;
	
	private Integer status;
	
	private ErroDetails erro_Deatils;

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public ErroDetails getErro_Deatils() {
		return erro_Deatils;
	}

	public void setErro_Deatils(ErroDetails erro_Deatils) {
		this.erro_Deatils = erro_Deatils;
	}

}
