package br.com.algartelecom.utils;

public class BadRequestException extends RuntimeException {

	private static final long serialVersionUID = 2702884477596879263L; 
	
	public BadRequestException (String msg) {
		super(msg);
	}

}