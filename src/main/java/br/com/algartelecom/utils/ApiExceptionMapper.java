package br.com.algartelecom.utils;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.sun.jersey.api.ParamException.QueryParamException;//NOSONAR

@Component
@Provider
@Produces(MediaType.APPLICATION_JSON)
public class ApiExceptionMapper implements ExceptionMapper<Exception>{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ApiExceptionMapper.class);
	
	public static final int OK = 200;
	public static final int CONFLICT = 409;
	public static final int UNPROCESSED_ENTITY = 422;
	public static final String INTERNAL_ERROR_MESSAGE = "A system error occurred";
	public static final String SERVICE_NOT_FOUND = "ServiÃ§o nÃ£o encontrado";

	@Context HttpHeaders headers;

	@Override
	@Produces(MediaType.APPLICATION_JSON)
	public Response toResponse(Exception exception) {
		
		LOGGER.error("Erro ao executar servico.", exception);
		
		Exception responseException = exception; 
		
		if(exception instanceof QueryParamException && exception.getCause() instanceof Exception) {
			responseException = (Exception) exception.getCause();
		}
		
		return buildResponseFromExceptions(responseException);		
	}

	private Response buildResponseFromExceptions(Exception responseException) {//NOSONAR
		if (responseException instanceof BadRequestException) {
			return badRequest(responseException);
		} else if (responseException instanceof NotFoundException) {
			return notFound(responseException.getMessage());
		} else if (responseException instanceof UnprocessedEntityException) {
			return unprocessedEntity( (UnprocessedEntityException) responseException );
		} else if (responseException instanceof WebApplicationException) {
			return notFound(SERVICE_NOT_FOUND);
		} else if(responseException instanceof IllegalArgumentException) {
			return badRequest(responseException);
		} else if (responseException instanceof JsonParseException){
			return badRequest(responseException);
		}else if (responseException instanceof JsonMappingException){
			return badRequest(responseException);
		}else if(responseException instanceof Exception){
			return exceptionCustom(responseException.getMessage());
		}
		return internalError(INTERNAL_ERROR_MESSAGE);
	}
	
	private Response unprocessedEntity(UnprocessedEntityException e){
		GenericJson<Object> g = new GenericJson<Object>();
		g.setMeta(new RestResponse(UNPROCESSED_ENTITY, e.getMessage()));
		g.setData(e.getData());
		return Response.status(UNPROCESSED_ENTITY).entity(g).type(MediaType.APPLICATION_JSON).build();
	}

	private Response badRequest(Exception e) {
		GenericJson<Object> g = new GenericJson<Object>();
		g.setMeta(new RestResponse(BAD_REQUEST.getStatusCode(), e.getMessage()));
		return Response.status(BAD_REQUEST).entity(g).type(MediaType.APPLICATION_JSON).build();
	}

	private Response notFound(String message) {
		GenericJson<Object> g = new GenericJson<Object>();
		g.setMeta(new RestResponse(NOT_FOUND.getStatusCode(), message));
		return Response.status(NOT_FOUND).entity(g).type(MediaType.APPLICATION_JSON).build();
	}
	
	private Response exceptionCustom(String message) {
		GenericJson<Object> g = new GenericJson<Object>();
		g.setMeta(new RestResponse(INTERNAL_SERVER_ERROR.getStatusCode(), message));
		return Response.status(INTERNAL_SERVER_ERROR).entity(g).type(MediaType.APPLICATION_JSON).build();
	}

	private Response internalError(String message) {
		GenericJson<Object> g = new GenericJson<Object>();
		g.setMeta(new RestResponse(INTERNAL_SERVER_ERROR.getStatusCode(), message));
		return Response.status(INTERNAL_SERVER_ERROR).entity(g).type(MediaType.APPLICATION_JSON).build();
	}
	
}