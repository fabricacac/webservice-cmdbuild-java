package br.com.algartelecom.utils;

public class NotFoundException extends RuntimeException {

	private static final long serialVersionUID = -981053780941770546L;

	public NotFoundException (String msg) {
		super(msg);
	}

}