package br.com.algartelecom.utils;

public class ErroDetails {
	
	private String erro_Code;
	
	private String erro_Desc;

	public String getErro_Code() {
		return erro_Code;
	}

	public void setErro_Code(String erro_Code) {
		this.erro_Code = erro_Code;
	}

	public String getErro_Desc() {
		return erro_Desc;
	}

	public void setErro_Desc(String erro_Desc) {
		this.erro_Desc = erro_Desc;
	}
	
	public static ErroDetails setObjectDetails(String code, String mensage){
		ErroDetails err = new ErroDetails();
		err.setErro_Code(code);
		err.setErro_Desc(mensage);
		return err;
	}

}
