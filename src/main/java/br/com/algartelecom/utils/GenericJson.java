package br.com.algartelecom.utils;

public class GenericJson<T> {

	protected RestResponse meta;

	protected T data;

	public RestResponse getMeta() {
		return meta;
	}

	public void setMeta(RestResponse meta) {
		this.meta = meta;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((meta == null) ? 0 : meta.hashCode());
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {//NOSONAR
		if (this == obj){
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		GenericJson<T> other = (GenericJson<T>) obj;
		if (data == null) {
			if (other.data != null){
				return false;
			}
		} else if (!data.equals(other.data)){
			return false;
		}
		if (meta == null) {
			if (other.meta != null){
				return false;
			}
		} else if (!meta.equals(other.meta)){
			return false;
		}
		return true;
	}
}