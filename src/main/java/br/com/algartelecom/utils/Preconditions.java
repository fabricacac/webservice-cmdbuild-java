package br.com.algartelecom.utils;

import org.apache.commons.lang.StringUtils;

public final class Preconditions {
	
	private Preconditions() { 
		
	}
	
	public static <T> T checkNotNull(T reference, String paramName) {
		if (reference == null) {
			throw new BadRequestException("Parâmetro "+paramName+" não pode ser nulo.");
		}
		return reference;
	}
	
	public static void checkArgument(boolean expression, String message) {
		if (!expression) {
			throw new BadRequestException(message);
		}
	}
	
	public static void checkNotBlank(String value, String paramName){
		checkArgument(StringUtils.isNotBlank(value), "Parâmetro " + paramName + " não pode ser vazio.");
	}

}
