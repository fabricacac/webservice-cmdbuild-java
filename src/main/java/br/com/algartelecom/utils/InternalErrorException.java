package br.com.algartelecom.utils;

public class InternalErrorException extends RuntimeException {

	private static final long serialVersionUID = -2034688696766167702L;

	public InternalErrorException (String msg) {
		super(msg);
	}

	public InternalErrorException(String message, Throwable cause) {
		super(message, cause);
	}
}