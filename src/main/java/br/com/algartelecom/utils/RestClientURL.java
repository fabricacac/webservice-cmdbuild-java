package br.com.algartelecom.utils;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class RestClientURL {

	private String url;

	private Client client;

	public RestClientURL(String host, Integer port, String type) {		
		this.url = type+"://"+host+":"+port;
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		clientConfig.getProperties().put(ClientConfig.PROPERTY_FOLLOW_REDIRECTS, true);
		client = Client.create(clientConfig);
		client.setFollowRedirects(true);
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public <T>ClientResponse post(WebResource service, T entity){
		return service.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.post(ClientResponse.class, entity);
	}

	public <T>ClientResponse put(String uri, T entity){
		return client.resource(uri).type(MediaType.APPLICATION_JSON)
				.put(ClientResponse.class, entity);
	}

	public<T>ClientResponse get(WebResource service, String idAuth){
		return service.type(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.header("CMDBuild-Authorization", idAuth)
				.get(ClientResponse.class);
	}
	
	public<T>ClientResponse post(WebResource service, String idAuth, T entity){
		return service.type(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.header("CMDBuild-Authorization", idAuth)
				.post(ClientResponse.class, entity);
	}
	
	public<T>ClientResponse put(WebResource service, String idAuth, T entity){
		return service.type(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.header("CMDBuild-Authorization", idAuth)
				.put(ClientResponse.class, entity);
	}

}
