package br.com.algartelecom.utils;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import com.sun.jersey.api.client.ClientHandler;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.ClientFilter;

public class RedirectFilter extends ClientFilter{

	@SuppressWarnings("deprecation")
	@Override
	public ClientResponse handle(ClientRequest cr) throws ClientHandlerException {
		ClientHandler ch = getNext();
		ClientResponse resp = ch.handle(cr);

		if (resp.getClientResponseStatus().getFamily() != Response.Status.Family.REDIRECTION) {
			return resp;
		}
		else {
			// try location
			String redirectTarget = resp.getHeaders().getFirst("Location");
			cr.setURI(UriBuilder.fromUri(redirectTarget).build());
			return ch.handle(cr);
		}
	}

}
