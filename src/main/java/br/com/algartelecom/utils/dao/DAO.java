package br.com.algartelecom.utils.dao;

import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.ejb.HibernateQuery;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.type.Type;

@SuppressWarnings("deprecation")
public abstract class DAO {

	protected void aliasToBean(Query query, Class<?> beanClass) {
		if(query instanceof HibernateQuery) {
			HibernateQuery hQuery = (HibernateQuery) query;
			org.hibernate.Query q = (org.hibernate.Query) hQuery.getHibernateQuery();
			q.setResultTransformer(new AliasToBeanResultTransformer(beanClass));
		} else {
			throw new IllegalArgumentException("A query não é do tipo HibernateQuery.");
		}
	}

	protected void addScalar(Query query, String alias) {
		addScalar(query, alias, null);
	}

	protected void addScalar(Query query, String alias, Type type) {
		if(query instanceof HibernateQuery) {
			HibernateQuery hQuery = (HibernateQuery) query;
			org.hibernate.Query q = (org.hibernate.Query) hQuery.getHibernateQuery();
			if(!(q instanceof SQLQuery)) {
				throw new IllegalArgumentException("A query não é nativa.");
			}
			SQLQuery sql = (SQLQuery) q;

			if(type != null) {
				sql.addScalar(alias, type);
			} else {
				sql.addScalar(alias);
			}
		} else {
			throw new IllegalArgumentException("A query não é do tipo HibernateQuery.");
		}
	}
}
