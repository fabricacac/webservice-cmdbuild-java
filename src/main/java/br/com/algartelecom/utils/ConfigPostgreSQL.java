package br.com.algartelecom.utils;

import org.springframework.stereotype.Component;

@Component
public class ConfigPostgreSQL {
	
	private String postgresql;

	public String getPostgresql() {
		return postgresql;
	}

	public void setPostgresql(String postgresql) {
		this.postgresql = postgresql;
	}

}
