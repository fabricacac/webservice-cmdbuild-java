package br.com.algartelecom.utils;

public abstract class Controller {

	public <T> GenericJson<T> ok(T model){
		GenericJson<T> genericJson = new GenericJson<T>();
		genericJson.setMeta(new RestResponse(200, ""));
		genericJson.setData(model);
		
		return genericJson;
	}
	
	public GenericJson<Object> ok(){
		GenericJson<Object> genericJson = new GenericJson<Object>();
		genericJson.setMeta(new RestResponse(200, ""));
		
		return genericJson;
	}
}